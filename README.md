# Code and data for "Students Attitudes Surrounding STEM: A Social Cognitive Career Theory Instrument for High School"

This repository contains the code and preprocessed data used to generate tables and figures for "Students Attitudes Surrounding STEM: A Social Cognitive Career Theory Instrument for High School", by EmilyKate McDonough, Ph.D., Kayle S. Sawyer, Ph.D., Jessica Wilks, Ph.D., and Berri Jacque, Ph.D., DOI: https://doi.org/10.1101/2021.11.29.470294

The code in this repository was written from 2020 through 2022 by 
Kayle S. Sawyer (email: kslays@bu.edu) and EmilyKate McDonough (email: emcdonou@gmail.com).
To the extent possible under law, the authors have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. The software is dedicated with the Creative Commons CC0 1.0 Universal Public Domain Dedication. This software is distributed without any warranty.
A copy of the CC0 Public Domain Dedication is included in this repository ("LICENSE.txt"). See <http://creativecommons.org/publicdomain/zero/1.0/>.

The R project ("SASS-validation.Rproj") was written to work with RStudio version 1.1.463. The working directory should be set to the directory that contains this file. Opening the R project file in RStudio will do that automatically.

The code ("R_code/SCCT-efa-cfa-validation.R" and "R_code/SCCT-test-retest.R") was written to work with R version 3.5.2 and 4.2.0.

There are four datasets in this repository, and all are in comma separated value format. Two of the datasets ("R_data/NewYork-Demographic-Factors.csv" and "R_data/selectedpopulations-2019-MA-DOE.csv") contain state level school socioeconomic data. These datasets were downloaded from https://data.nysed.gov (downloaded on Oct 20, 2020) and https://profiles.doe.mass.edu (downloaded on Oct 19, 2020), respectively. The remaining two datasets ("R_data/STEM-SCCT-HS.csv" and "R_data/test-retest.csv") have been pre-processed to remove identifying information of study participants and they contain demographic attributes and questionnaire responses.

Descriptions of the column variables from the main dataset ("R_data/STEM-SCCT-HS.csv") are as follows:

school: deidentified school code

Duration (in seconds): length of time participant took to respond to survey

Q31: responses to: "Where are you taking this survey?" (levels are: 'At school in a class', 'At school on my own time', 'At home', 'Other (please specify)')

Q31_4_TEXT: participant write-in response to 'Other (please specify)' from question Q31

Q32: responses to: "Are you taking this survey on a computer or tablet or phone?" (levels are: 'Computer', 'Tablet', 'Phone')

Q6: responses to: "What grade are you in?" (levels are: '7th', '8th', '9th', '10th', '11th', '12th')

Q7: responses to: "What science classes have you taken?" (mark all that apply: 'Earth science', 'Biology', 'Chemistry', 'Physics', 'An elective science course', 'One or more AP/advanced/honors science courses')

Q8: responses to: "Have you participated in science activities outside the classroom?" (levels are: 'Yes', 'No', 'If yes, please specify'). Note that written in responses were removed from the dataset as they contained identifying information.

Q3_1 to Q16_10: participant responses to survey items (6 level scale)

Q17: responses to "Have you heard of Tufts university?" (levels are: 'Yes', 'No')

Q18: responses to: "I intend to go to college:" (levels are: 'Yes', 'Maybe', 'No')

Q23: responses to: "I intend to pursue a career related to science, technology, engineering, or math (STEM)." (levels are: 'Yes', 'Maybe', 'No')

Q24: responses to: "My awareness of what I need to do to get a science, technology, engineering, and/or math (STEM) related job has changed in the last year." (levels are: 'Yes', 'No', 'If yes, please explain')

Q24_3_TEXT: write-in response to Q24 'If yes, please explain'

Q25: responses to: "I have a better idea of what a science, technology, engineering, and/or math (STEM) related job looks like than I did last year."  (levels are: 'Yes', 'No', 'If yes, please explain')

Q25_3_TEXT: write-in response to Q25 'If yes, please explain'

Q19_1: responses to: "My grades in school are mostly: English classes" (levels are: 'A', 'B', 'C', 'D')

Q19_2: responses to: "My grades in school are mostly: History classes" (levels are: 'A', 'B', 'C', 'D')

Q19_3: responses to: "My grades in school are mostly: Math classes" (levels are: 'A', 'B', 'C', 'D')

Q19_4: responses to: "My grades in school are mostly: Foreign language classes" (levels are: 'A', 'B', 'C', 'D')

Q19_5: responses to: "My grades in school are mostly: Biology classes" (levels are: 'A', 'B', 'C', 'D')

Q19_6: responses to: "My grades in school are mostly: Chemistry classes" (levels are: 'A', 'B', 'C', 'D')

Q19_7: responses to: "My grades in school are mostly: Art/music/theater/dance classes" (levels are: 'A', 'B', 'C', 'D')

Q19_8: responses to: "My grades in school are mostly: Physics classes" (levels are: 'A', 'B', 'C', 'D')

Q20: responses to: "I identify as:" (levels are: 'Female', 'Male', 'Other/non-binary', 'Prefer not to answer')

Q21: responses to: "What is your race? Select one or more responses." (levels are: 'American Indian or Alaskan Native', 'Asian', 'Black or African American', 'Native Hawaiian or Other Pacific Islander', 'White', 'Prefer not to answer')

Q22: responses to: "Are you Hispanic or Latino/a?" (levels are: 'Yes', 'No', 'Prefer not to answer')

PER_ECDIS: percent of students within the school who are economically disadvantaged as determined by MA or NY state criteria (see manuscript for more detail)


Descriptions of the column variables from the test-retest dataset ("R_data/test-retest.csv") are as follows:

Duration (in seconds): length of time participant took to respond to survey

Q31: responses to: "Where are you taking this survey?" (levels are: 'At school in a class', 'At school on my own time', 'At home', 'Other (please specify)')

Q31_4_TEXT: participant write-in response to 'Other (please specify)' from question Q31

Q32: responses to: "Are you taking this survey on a computer or tablet or phone?" (levels are: 'Computer', 'Tablet', 'Phone')

Q6: responses to: "What grade are you in?" (levels are: '7th', '8th', '9th', '10th', '11th', '12th')

Q7: responses to: "What science classes have you taken?" (mark all that apply: 'Earth science', 'Biology', 'Chemistry', 'Physics', 'An elective science course', 'One or more AP/advanced/honors science courses')

Q8: responses to: "Have you participated in science activities outside the classroom?" (levels are: 'Yes', 'No', 'If yes, please specify'). Note that written in responses were removed from the dataset as they contained identifying information.

Q3_1 to Q16_10: participant responses to survey items (6 level scale)

Q17: responses to "Have you heard of Tufts university?" (levels are: 'Yes', 'No')

Q18: responses to: "I intend to go to college:" (levels are: 'Yes', 'Maybe', 'No')

Q23: responses to: "I intend to pursue a career related to science, technology, engineering, or math (STEM)." (levels are: 'Yes', 'Maybe', 'No')

Q19_1: responses to: "My grades in school are mostly: English classes" (levels are: 'A', 'B', 'C', 'D')

Q19_2: responses to: "My grades in school are mostly: History classes" (levels are: 'A', 'B', 'C', 'D')

Q19_3: responses to: "My grades in school are mostly: Math classes" (levels are: 'A', 'B', 'C', 'D')

Q19_4: responses to: "My grades in school are mostly: Foreign language classes" (levels are: 'A', 'B', 'C', 'D')

Q19_5: responses to: "My grades in school are mostly: Biology classes" (levels are: 'A', 'B', 'C', 'D')

Q19_6: responses to: "My grades in school are mostly: Chemistry classes" (levels are: 'A', 'B', 'C', 'D')

Q19_7: responses to: "My grades in school are mostly: Art/music/theater/dance classes" (levels are: 'A', 'B', 'C', 'D')

Q19_8: responses to: "My grades in school are mostly: Physics classes" (levels are: 'A', 'B', 'C', 'D')

Q20: responses to: "I identify as:" (levels are: 'Female', 'Male', 'Other/non-binary', 'Prefer not to answer')

Q21: responses to: "What is your race? Select one or more responses." (levels are: 'American Indian or Alaskan Native', 'Asian', 'Black or African American', 'Native Hawaiian or Other Pacific Islander', 'White', 'Prefer not to answer')

Q22: responses to: "Are you Hispanic or Latino/a?" (levels are: 'Yes', 'No', 'Prefer not to answer')

school: deidentified school code

timePoint: indicates which time point (1 or 2) the response is from

id: participant id, created for timepoint matching

timeDiff: time difference between first and second recorded response, rounded to the nearest day
